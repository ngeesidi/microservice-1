package com.mss.m1.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface M1Repository extends JpaRepository<M1, Integer> {
	M1 findById(int id);

	String findNameById(int id);
}
